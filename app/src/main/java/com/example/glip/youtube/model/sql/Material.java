package com.example.glip.youtube.model.sql;


import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Material.TABLE_NAME_VIDEOS)
public class Material {
    public static final String TABLE_NAME_VIDEOS = "videos";
    public static final String FIELD_NAME_ID     = "id";
    public static final String FIELD_NAME_UID   = "UID";
    public static final String FIELD_NAME_TITLE = "title";
    public static final String FIELD_NAME_DURATION     = "duration";
    public static final String FIELD_NAME_ID_GROUP = "Id group";
    public static final String FIELD_NAME_ICON_URL = "Icon_url";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int mId;

    @SerializedName("uid")
    @DatabaseField(columnName = FIELD_NAME_UID)
    private String uid;

    @DatabaseField(columnName = FIELD_NAME_TITLE)
    @SerializedName("title")
    private String title;

    @DatabaseField(columnName = FIELD_NAME_DURATION)
    @SerializedName("duration")
    private String duration;

    @DatabaseField (columnName = FIELD_NAME_ID_GROUP)
    private Integer idGroup;

    @DatabaseField(columnName = FIELD_NAME_ICON_URL)
    @SerializedName("icon_url")
    private String iconURL;

    public Material() {

    }

    public int getmId() {
        return mId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }
}
