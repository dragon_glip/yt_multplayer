package com.example.glip.youtube.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.glip.youtube.R;
import com.google.android.youtube.player.YouTubeThumbnailView;

public class YoutubeViewHolder extends RecyclerView.ViewHolder {

    public ImageView videoImageView;
    public TextView videoTitle, videoDuration;

    public YoutubeViewHolder(View itemView) {
        super(itemView);
        videoImageView = itemView.findViewById(R.id.variable_materials_image_view);
        videoTitle = itemView.findViewById(R.id.video_title_label);
        videoDuration = itemView.findViewById(R.id.video_duration_label);
    }
}
