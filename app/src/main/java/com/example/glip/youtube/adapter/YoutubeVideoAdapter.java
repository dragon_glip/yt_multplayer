package com.example.glip.youtube.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.glip.youtube.DownloadImageTask;
import com.example.glip.youtube.R;

import com.example.glip.youtube.holder.YoutubeViewHolder;
import com.example.glip.youtube.model.sql.Material;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;

public class YoutubeVideoAdapter extends RecyclerView.Adapter<YoutubeViewHolder> {
    private static final String TAG = YoutubeVideoAdapter.class.getSimpleName();
    private Context context;
    String link = "";

    private ArrayList<Material> materials;
    private static final String API_KEY = "AIzaSyBVe6FJXkdd2ZMXdwiOjLksVX4Bd0HZFBQ";


    public YoutubeVideoAdapter(Context context, ArrayList<Material> materials) {
        this.context = context;

        this.materials = materials;

    }

    @NonNull
    @Override
    public YoutubeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.variable_materials_card, parent, false);
        return new YoutubeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YoutubeViewHolder holder, int position) {
        final Material dataVideo = materials.get(position);
        holder.videoTitle.setText(dataVideo.getTitle());
        holder.videoDuration.setText(dataVideo.getDuration());
        link = dataVideo.getIconURL();
        new DownloadImageTask(holder.videoImageView)
                .execute(link);
    }

    @Override
    public int getItemCount() {
        return materials != null ? materials.size() : 0;
    }

}
