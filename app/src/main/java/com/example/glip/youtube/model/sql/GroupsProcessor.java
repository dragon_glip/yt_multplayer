package com.example.glip.youtube.model.sql;

import android.content.Context;

import com.example.glip.youtube.model.Groups;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

public class GroupsProcessor {
    Dao<Material, Integer> mMaterialsDao;
    Dao<MapGroupMaterials, Integer> mMapsDao;
    DataBaseHelper helper ;
    Dao<Group, Integer> groupDao;

    public GroupsProcessor(){
        groupDao = null;
        mMapsDao = null;
        mMaterialsDao = null;

    }

    public void groupList(Groups groups, Context context) {
        helper = new DataBaseHelper(context);
        try {
            if (groups != null) {
                groupDao = helper.getGroupsDao();
                for (int i = 0; i < groups.getGroups().size(); i++) {
                    Group mgroup = new Group();
                    Integer id = groups.getGroups().get(i).getmId();
                    mgroup.setmId(id);
                    mgroup.setTitle(groups.getGroups().get(i).getTitle());

                    if (groupDao.queryBuilder()
                            .where()
                            .eq(Group.FIELD_NAME_ID, id)
                            .countOf() == 0) {
                        groupDao.create(mgroup);
                    }
                   if (mgroup.getMaterials() != null) {
                        mMaterialsDao = helper.getMaterialsDao();
                        mMapsDao = helper.getmMapsDao();
                        for (int j = 0; j < mgroup.getMaterials().size(); j++) {
                            Material material = mgroup.getMaterials().get(j);
                            int groupId = groups.getGroups().get(i).getmId();
                            setMaterialInBase(material, groupId);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setMaterialInBase(Material material, int groupId) {

        MapGroupMaterials mapGroupMaterials = new MapGroupMaterials();
        String uid = material.getUid();
        try {
            if (mMaterialsDao.queryBuilder()
                    .where()
                    .eq(Material.FIELD_NAME_UID, uid)
                    .countOf() == 0) {
                mMaterialsDao.create(material);
            }

            mapGroupMaterials.setMaterialsId(material.getmId());
            mapGroupMaterials.setGroupId(groupId);
            mMapsDao.create(mapGroupMaterials);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
