package com.example.glip.youtube;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.glip.youtube.adapter.YoutubeVideoAdapter;
import com.example.glip.youtube.model.sql.Material;
import com.example.glip.youtube.utils.RecyclerViewOnClickListener;

import java.util.ArrayList;

public class VariableMaterials extends Fragment{

    ArrayList<Material> actualMaterialList;
    ArrayList<Material> fullMaterialList;
    FragmentManager fragmentManager;
    View rootView;
    PlayerListFragment playerListFragment;

    public VariableMaterials() {
        actualMaterialList = null;
        fullMaterialList= null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.variable_materials_fragment, container, false);

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        YoutubeVideoAdapter adapter = new YoutubeVideoAdapter(getActivity(), actualMaterialList);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerViewOnClickListener(getActivity(), new RecyclerViewOnClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                YouTubeFragment youTubeFragment = new YouTubeFragment();
                Bundle bundle = new Bundle();
                String video_id = actualMaterialList.get(position).getUid();

                bundle.putString("video_id", video_id);
                youTubeFragment.setArguments(bundle);


                ArrayList<Material> uids = new ArrayList<Material>();

                for(int i = 0; i< fullMaterialList.size(); i++){
                   String uid = fullMaterialList.get(i).getUid();
                   Material material = fullMaterialList.get(i);
                    if (uid.compareTo(video_id) != 0){

                        uids.add(material);
                   }
                }

                playerListFragment.setActualMaterialList(uids);
                playerListFragment.startYouTubTrumbernails();


                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.youtube_viwer, youTubeFragment)
                        .addToBackStack(null)
                        .commit();

            }
        }));
        return rootView;
    }


    public void setActualListMaterials(ArrayList<Material> actualMaterialList) {
        this.actualMaterialList = actualMaterialList;
    }

    public void setFullListMaterials(ArrayList<Material> fullMaterialList) {
        this.fullMaterialList = fullMaterialList;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void setPlayerListFragment (PlayerListFragment playerListFragment){
        this.playerListFragment =  playerListFragment;
    }
}

