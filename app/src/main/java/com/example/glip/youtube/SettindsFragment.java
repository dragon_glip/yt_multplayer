package com.example.glip.youtube;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES;
import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES_TIME_LIMIT;


public class SettindsFragment extends Fragment {
    View rootView;
    private SharedPreferences mSettings;
    long timelimit;
    EditText editText;

    public SettindsFragment() {
        editText = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mSettings = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settings, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        editText = rootView.findViewById(R.id.editText);
        SeekBar seekBar = rootView.findViewById(R.id.seekBar);
        final TextView textView = rootView.findViewById(R.id.textView2);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                textView.setText(String.valueOf(progress) + " " + getString(R.string.min));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        /*try {timelimit = Integer.parseInt(editText.getText().toString());}
        catch (Exception e){
            e.printStackTrace();
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        timelimit = TimeUnit.MINUTES.toMillis(Integer.parseInt(editText.getText().toString()));
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong(APP_PREFERENCES_TIME_LIMIT, timelimit);
        editor.apply();
    }

    public static SettindsFragment newInstance() {
        return new SettindsFragment();
    }
}
