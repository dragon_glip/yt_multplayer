package com.example.glip.youtube.model;

import com.google.gson.Gson;

public class ParserJson {

    Gson gson = new Gson();
    Materials dataVideoModel = null;

    Groups groups = null;

    public Materials parseJsonMaterials (String jsonString) {

        try {
            dataVideoModel = gson.fromJson(jsonString, Materials.class);
            return dataVideoModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Groups parseJsonGroups(String jsonString) {

        try {
            groups = gson.fromJson(jsonString, Groups.class);
            return groups;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}