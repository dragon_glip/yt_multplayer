package com.example.glip.youtube;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.glip.youtube.adapter.MainGroupAdapter;
import com.example.glip.youtube.model.Groups;
import com.example.glip.youtube.model.ParserJson;
import com.example.glip.youtube.model.sql.Group;
import com.example.glip.youtube.model.sql.GroupsProcessor;
import com.example.glip.youtube.model.sql.Material;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class FragmentContainerActivity extends AppCompatActivity {
    VariableMaterials variableMaterials;
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_TIME_LIMIT = "limit_time";
    public static final String APP_PREFERENCES_TIME_CURRENT = "current_time";
    public static final String APP_PREFERENCES_TIME_STAMP_START = "time_stamp_start";
    private SharedPreferences mSettings;
    long timelimit;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;


    FragmentManager fragmentManager;
    Groups resultGroups;
    GroupsProcessor groupsProcessor = new GroupsProcessor();
    List<Group> myGroups;
    List<Material> materialList;

    String resultJsonSring;
    static final String JSON_STRING = "Actual json string";

    PlayerListFragment playerListFragment;

    public FragmentContainerActivity() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        timelimit = 0;
        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        fragmentManager = getSupportFragmentManager();
        if (savedInstanceState != null) {
            resultJsonSring = savedInstanceState.getString(JSON_STRING);
        } else {
            Intent intent = getIntent();
            resultJsonSring = intent.getStringExtra("groupsJson");
            groupsToFragment(resultJsonSring);
        }
        try {
            if (resultGroups != null) {
                groupsProcessor.groupList(resultGroups, this);
                myGroups = resultGroups.getGroups();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MainGroupAdapter adapter = new MainGroupAdapter(this, myGroups, this);
        recyclerView.setAdapter(adapter);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.action_humburher:

                        selectedFragment = SettindsFragment.newInstance();

                        break;
                    case R.id.action_favorites:

                        break;
                    case R.id.action_search:

                        break;

                }
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_layout, selectedFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void groupsToFragment(String resultJson) {
        ParserJson parserJson = new ParserJson();
        resultGroups = parserJson.parseJsonGroups(resultJson);
    }

    public void startPlayerListFragment (String video_id, ArrayList<Material> materialList){
       if (timelimit != 0){
        mTimer.schedule(mMyTimerTask, timelimit);
       }
        playerListFragment = new PlayerListFragment();
        playerListFragment.setFragmentManager(fragmentManager);
        playerListFragment.setFullMaterialList(materialList);
        Bundle bundle = new Bundle();
        bundle.putString("video_id", video_id);

        ArrayList<Material> uids = new ArrayList<Material>();

        for(int i = 0; i< materialList.size(); i++){
            String uid = materialList.get(i).getUid();
            Material material = materialList.get(i);
            if (uid.compareTo(video_id) != 0){

                uids.add(material);
            }
        }
        playerListFragment.setActualMaterialList(uids);
        playerListFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction;
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_layout, playerListFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(JSON_STRING, resultJsonSring);
        super.onSaveInstanceState(outState);
    }

    class MyTimerTask extends TimerTask{

        @Override
        public void run() {
            //Toast.makeText(getApplicationContext(), "Time Limit", Toast.LENGTH_SHORT).show();
            PauseFragment pauseFragment = new PauseFragment();
            FragmentTransaction fragmentTransaction;
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_layout, pauseFragment)
                    .addToBackStack(null)
                    .commit();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE); //Создание файла настроек
        if (mSettings.contains(APP_PREFERENCES_TIME_LIMIT)){
            timelimit = mSettings.getLong(APP_PREFERENCES_TIME_LIMIT, 0);


        }

    }

}
