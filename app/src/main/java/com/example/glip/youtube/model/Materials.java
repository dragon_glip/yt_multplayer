package com.example.glip.youtube.model;

import com.example.glip.youtube.model.sql.Material;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Materials {
    @SerializedName("data")
    private List<Material> dataVideos = new ArrayList<Material>();

    public List<Material> getDataVideos() {
        return dataVideos;
    }

    public void setDataVideos(List<Material> dataVideos) {
        this.dataVideos = dataVideos;
    }
}
