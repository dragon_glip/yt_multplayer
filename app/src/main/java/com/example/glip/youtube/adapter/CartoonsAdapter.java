package com.example.glip.youtube.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.glip.youtube.DownloadImageTask;
import com.example.glip.youtube.R;
import com.example.glip.youtube.holder.CartoonsHolder;
import com.example.glip.youtube.model.sql.Material;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

public class CartoonsAdapter extends RecyclerView.Adapter<CartoonsHolder> {
    private Context context;
    private List<Material> materialList;
    String link = "";

    public CartoonsAdapter (Context context, List<Material> materialList){
       this.context = context;
       this.materialList = materialList;
    }
    @NonNull
    @Override
    public CartoonsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_cartoon, parent, false);

        return new CartoonsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartoonsHolder holder, int position) {
        final Material material = materialList.get(position);
        holder.titleCartoon.setText(material.getTitle());
        link = material.getIconURL();
        new DownloadImageTask(holder.cartoonPreview)
                .execute(link);

    }


    @Override
    public int getItemCount() {
        return materialList != null ? materialList.size() : 0;
    }


}
