package com.example.glip.youtube;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.glip.youtube.model.sql.Material;

import java.util.ArrayList;
import java.util.List;

import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES_TIME_LIMIT;
import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES_TIME_STAMP_START;

public class PlayerListFragment extends Fragment {
    FragmentManager fragmentManager;
    ArrayList<Material> fullMaterialList;
    ArrayList<Material> actualMaterialList;
    YouTubeFragment youTubeFragment;
    VariableMaterials variableMaterials;
    private SharedPreferences mSettings;
    List<String> newMaterialsList;
    private static final String API_KEY = "AIzaSyBVe6FJXkdd2ZMXdwiOjLksVX4Bd0HZFBQ";
    long ts;
    int timelimit;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.youtube_player_list, container, false);


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        String video_id = getArguments().getString("video_id");
        Bundle bundle = new Bundle();
        bundle.putString("video_id", video_id);
        youTubeFragment = new YouTubeFragment();
        youTubeFragment.setArguments(bundle);
        youTubeFragment.setFragmentManager(fragmentManager);
        startYouTubTrumbernails();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.youtube_viwer, youTubeFragment)

                            .commit();
       super.onActivityCreated(savedInstanceState);
    }

    public void setFragmentManager (FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    public void setActualMaterialList(ArrayList<Material> actualMaterialList) {
        this.actualMaterialList = actualMaterialList;
    }

    public void setFullMaterialList(ArrayList<Material> fullMaterialList){
        this.fullMaterialList = fullMaterialList;
    }


    public void startYouTubTrumbernails (){
        variableMaterials = new VariableMaterials();
        variableMaterials.setActualListMaterials(actualMaterialList);
        variableMaterials.setFullListMaterials(fullMaterialList);
        variableMaterials.setFragmentManager(fragmentManager);
        variableMaterials.setPlayerListFragment(this);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.youtube_thumbernails, variableMaterials)
                .commit();
    }

    @Override
    public void onStop() {

        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
