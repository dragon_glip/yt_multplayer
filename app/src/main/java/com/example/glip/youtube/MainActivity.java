package com.example.glip.youtube;


import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class MainActivity extends AppCompatActivity {
    View rootView;
    Handler handler;

    FragmentManager fragmentManager;
    final int STRING_JSON_GROUP = 0; //пришла jsonGroups строка

    FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);


        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case STRING_JSON_GROUP:
                        String finalResultJson = (String) msg.obj;
                        Intent i = new Intent(MainActivity.this, FragmentContainerActivity.class);
                        i.putExtra("groupsJson", finalResultJson);
                        startActivity(i);

                        break;
                }
            }
        };


        new Thread(new Runnable() {
            public void run() {
                BufferedReader reader;
                Message msg;
                try {
                    URL url = new URL("https://gist.githubusercontent.com/shkuropat/6a89b91e7cdc1ce4cd4bac01bf75ce43/raw/2111139d3a66fa380da19e0933014b49e813afe6/groups_v2.json");
                    URLConnection urlConnection = url.openConnection();
                    urlConnection.connect();
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    final String resultJson = buffer.toString();
                    msg = handler.obtainMessage(STRING_JSON_GROUP , resultJson);
                    handler.sendMessage(msg);

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }).start();





    }
}
