package com.example.glip.youtube.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.glip.youtube.R;

public class MainGroupHolder extends RecyclerView.ViewHolder {
    public TextView titleGroup;
    public RecyclerView recyclerView;

    public MainGroupHolder(View itemView) {
        super(itemView);
        titleGroup = itemView.findViewById(R.id.textView_titlegroup);
        recyclerView = itemView.findViewById(R.id.recycler_view_cartoons);
    }
}
