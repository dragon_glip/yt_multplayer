package com.example.glip.youtube;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.sql.Timestamp;

import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES;
import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES_TIME_CURRENT;
import static com.example.glip.youtube.FragmentContainerActivity.APP_PREFERENCES_TIME_STAMP_START;


public class YouTubeFragment extends Fragment{
    FragmentManager fragmentManager;

    private static final String API_KEY = "AIzaSyBVe6FJXkdd2ZMXdwiOjLksVX4Bd0HZFBQ";

    long milliSecStopPlayer;
    SharedPreferences mSettings;
    long ts;
    public YouTubeFragment (){


    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.youtube_player, container, false);
        try{
        mSettings = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);}
        catch (NullPointerException e){
            e.printStackTrace();
        }

        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();
        ts = System.currentTimeMillis();
        youTubePlayerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean wasRestored) {
                if (!wasRestored) {
                    Bundle bundle = getArguments();
                    if (bundle != null) {
                        String video_id = bundle.getString("video_id");
                        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                        youTubePlayer.loadVideo(video_id);
                        youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
                            @Override
                            public void onPlaying() {

                            }

                            @Override
                            public void onPaused() {

                            }

                            @Override
                            public void onStopped() {
                                milliSecStopPlayer = youTubePlayer.getCurrentTimeMillis();
                                SharedPreferences.Editor editor = mSettings.edit();
                                editor.putLong(APP_PREFERENCES_TIME_CURRENT, milliSecStopPlayer);
                                editor.putLong(APP_PREFERENCES_TIME_STAMP_START, ts);
                                editor.apply();
                            }

                            @Override
                            public void onBuffering(boolean b) {

                            }

                            @Override
                            public void onSeekTo(int i) {

                            }
                        });
                        youTubePlayer.play();
                    }
                }
            }
            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                String errorMessage = youTubeInitializationResult.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }
        });

        super.onActivityCreated(savedInstanceState);
    }

    public void setFragmentManager (FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }
    public long getMilliSecStopPlayer (){
        return milliSecStopPlayer;
    }
    @Override
    public void onStop() {
        super.onStop();

    }
}
