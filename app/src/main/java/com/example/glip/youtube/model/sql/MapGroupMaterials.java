package com.example.glip.youtube.model.sql;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = MapGroupMaterials.TABLE_NAME_MAP)
public class MapGroupMaterials {

    public static final String TABLE_NAME_MAP = "MapGroupMaterials";
    public static final String FIELD_NAME_GROUP_ID     = "Group id";
    public static final String FIELD_NAME_MATERIALS_ID     = "Material id";

    @DatabaseField(columnName = FIELD_NAME_GROUP_ID)
    private int groupId;

    @DatabaseField(columnName = FIELD_NAME_MATERIALS_ID)
    private int materialsId;

    public MapGroupMaterials (){}

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getMaterialsId() {
        return materialsId;
    }

    public void setMaterialsId(int materialsId) {
        this.materialsId = materialsId;
    }
}
