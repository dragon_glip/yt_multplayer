package com.example.glip.youtube.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.glip.youtube.FragmentContainerActivity;
import com.example.glip.youtube.R;
import com.example.glip.youtube.holder.MainGroupHolder;
import com.example.glip.youtube.model.sql.Group;
import com.example.glip.youtube.model.sql.Material;
import com.example.glip.youtube.utils.RecyclerViewOnClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainGroupAdapter extends RecyclerView.Adapter<MainGroupHolder> {
    private Context context;
    //private  Groups groups;
    private List<Group> groupList;
    FragmentContainerActivity fragmentContainerActivity;

    int groupID;


    public MainGroupAdapter(Context context, List<Group> groupList, FragmentContainerActivity fragmentContainerActivity) {
        this.groupList = groupList;
        this.context = context;
        this.fragmentContainerActivity = fragmentContainerActivity;

    }

    @NonNull
    @Override
    public MainGroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.main_card, parent, false);
        return new MainGroupHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainGroupHolder holder, int position) {
        final Group group = groupList.get(position);
        holder.titleGroup.setText(group.getTitle());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        final ArrayList<Material> materialList = group.getMaterials();
        CartoonsAdapter adapter = new CartoonsAdapter(context, materialList);
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.addOnItemTouchListener(new RecyclerViewOnClickListener(context, new RecyclerViewOnClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String video_id = materialList.get(position).getUid();
               fragmentContainerActivity.startPlayerListFragment(video_id, materialList);
            }
        }));
    }






    @Override
    public int getItemCount() {
        return groupList != null ? groupList.size() : 0;
    }


}
