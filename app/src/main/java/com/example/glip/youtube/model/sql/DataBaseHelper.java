package com.example.glip.youtube.model.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME    = "ormlite3.db";
    private static final int    DATABASE_VERSION = 1;
    private Dao<Material, Integer> mMaterialsDao = null;
    private Dao<Group, Integer> mGroupsDao = null;
    private Dao<MapGroupMaterials, Integer> mMapsDao = null;

    public DataBaseHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Material.class);
            TableUtils.createTable(connectionSource, Group.class);
            TableUtils.createTable(connectionSource, MapGroupMaterials.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Material.class, true);
            TableUtils.dropTable(connectionSource, Group.class, true);
            TableUtils.dropTable(connectionSource, MapGroupMaterials.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public Dao<Material, Integer> getMaterialsDao() throws SQLException {
        if (mMaterialsDao == null) {
            mMaterialsDao = getDao(Material.class);
        }

        return mMaterialsDao;
    }

    public Dao<Group, Integer> getGroupsDao() throws SQLException {
        if (mGroupsDao == null) {
            mGroupsDao = getDao(Group.class);
        }

        return mGroupsDao;
    }

    public Dao<MapGroupMaterials, Integer> getmMapsDao() throws SQLException {
        if (mMapsDao == null) {
            mMapsDao = getDao(MapGroupMaterials.class);
        }

        return mMapsDao;
    }

    @Override
    public void close() {
        mMaterialsDao = null;
        mGroupsDao = null;
        mMapsDao = null;
        super.close();
    }
}
