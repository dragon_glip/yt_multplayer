package com.example.glip.youtube.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.glip.youtube.R;

public class CartoonsHolder extends RecyclerView.ViewHolder {
    public ImageView cartoonPreview;
    public TextView titleCartoon;


    public CartoonsHolder(View itemView) {
        super(itemView);
        cartoonPreview = itemView.findViewById(R.id.image_view_cartoon);
        titleCartoon = itemView.findViewById(R.id.textView_cartoon);
    }
}
