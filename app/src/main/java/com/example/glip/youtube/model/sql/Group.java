package com.example.glip.youtube.model.sql;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;


@DatabaseTable(tableName = Group.TABLE_NAME_GROUPS)
public class Group {
    public static final String TABLE_NAME_GROUPS = "Groups";
    public static final String FIELD_NAME_ID     = "id";
   // public static final String FIELD_NAME_ICONURL = "Icon URL";
    public static final String FIELD_NAME_TITLE = "title";


    @SerializedName("id")
    @DatabaseField(columnName = FIELD_NAME_ID)
    private int mId;

    @DatabaseField(columnName = FIELD_NAME_TITLE)
    @SerializedName("title")
    private String title;

    @SerializedName("materials")
    public ArrayList<Material> materials;

   /* @SerializedName("icon_url")
    @DatabaseField(columnName = FIELD_NAME_ICONURL)
    private String icon_url;
*/


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public ArrayList<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(ArrayList<Material> materials) {
        this.materials = materials;
    }

    public  Group (){

    }

}
